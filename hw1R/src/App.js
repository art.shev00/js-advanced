import React, { useState } from 'react';
import Button from './components/Button';
import Modal from './components/Modal';

class App extends React.Component {
  constructor() {
    super()

    this.state = {
      openModal1: false,
      openModal2: false
    }
  }

  closeModal1 = () => {
    this.setState({
      openModal1: false
    })
  }
  closeModal2 = () => {
    this.setState({
      openModal2: false
    })
  }
  render() {
    return (
      <>
        <div className='buttons-container'>
          <Button
            text='Open first modal'
            color='blue'
            onClick={() => { this.setState({ openModal1: true }) }}
          />
          <Button
            text='Open second modal'
            color='black'
            onClick={() => { this.setState({ openModal2: true }) }}
          />

          <Modal
            closeModal={this.closeModal1}
            open={this.state.openModal1}
            header="Some title" closeButton={true}
            text="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"
          />
          <Modal
            closeModal={this.closeModal2}
            open={this.state.openModal2}
            header="Facking modals" closeButton={true}
            text="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.  and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"
          />
        </div>

      </>
    )
  }
}

export default App;
