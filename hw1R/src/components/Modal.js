import React from 'react';
import Button from './Button';
import './modal.scss'

class Modal extends React.Component {
    constructor(props) {
        super(props)

    }
    render() {
        let { open, closeModal } = this.props
        if (!open) {
            return null
        } else {

            return (
                <>
                    <div id={this.props.id} className='overlay' onClick={closeModal}>
                        <div onClick={(e) => {
                            e.stopPropagation()
                        }} className='modal-container'>
                            <div className='modal-header'>
                                <p className='close-btn' onClick={closeModal} >x</p>
                                <h1>{this.props.header}</h1>
                            </div>
                            <div className='modal-text'>
                                <p>{this.props.text}</p>
                            </div>
                            <div className='buttons'>
                                <Button onClick={closeModal} text='OK' color='green' />
                                <Button onClick={closeModal} text='Cancel' color='red' />
                            </div>
                        </div>
                    </div>
                </>
            )
        }

    }
}
export default Modal;