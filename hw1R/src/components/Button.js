import React from 'react';
import './button.scss'

class Button extends React.Component {
    constructor(props) {
        super(props)

        this.bacColor = props.color;
        this.text = props.text;
        this.fnc = props.onClick

    }

    render() {
        return (
            <>
                <button className='btn' onClick={this.fnc} style={{ backgroundColor: this.bacColor }}>{this.text}</button>
            </>
        )
    }
}

export default Button